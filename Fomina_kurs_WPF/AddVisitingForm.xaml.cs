﻿using Fomina_kurs_WPF.Models;
using Fomina_kurs_WPF.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Fomina_kurs_WPF
{
    /// <summary>
    /// Логика взаимодействия для AddVisitingForm.xaml
    /// </summary>
    public partial class AddVisitingForm : Window
    {
        //FitnessContext db;
        public AddVisitingForm(int abonId)
        {
            InitializeComponent();
            //db = dbContext;
            //comboBoxPeriods.ItemsSource = db.PERIOD.ToList();
            DataContext = new AddVisitingModel(abonId);
            datePicker.DisplayDateStart = DateTime.Now;
            datePicker.DisplayDateEnd = DateTime.Now.AddDays(14);
        }

        public AddVisitingForm(int abonId, int period_id, int trainer_id, DateTime dateTime)
        {
            InitializeComponent();
            //db = dbContext;
            //comboBoxPeriods.ItemsSource = db.PERIOD.ToList();
            DataContext = new AddVisitingModel(abonId, period_id, dateTime);
            datePicker.DisplayDateStart = DateTime.Now;
            datePicker.DisplayDateEnd = DateTime.Now.AddDays(14);
        }

        private void InsertVisiting(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
            this.Close();
        }

        private void calendar_SelectedDatesChanged(object sender, SelectionChangedEventArgs e)
        {

        }
    }
}
