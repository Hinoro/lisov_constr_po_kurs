﻿using Fomina_kurs_WPF.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Fomina_kurs_WPF
{
    /// <summary>
    /// Логика взаимодействия для Login.xaml
    /// </summary>
    public partial class Login : Window
    {
        FitnessContext db;
        public Login()
        {
            db = new FitnessContext();
            InitializeComponent();
        }

        private void btnSubmit_Click(object sender, RoutedEventArgs e)
        {
            var request = db.User.ToList().Where(i => i.Login == txtUsername.Text && i.Password == txtPassword.Password).Count();
            if (request == 1)
            {
                MainWindow mainWindow = new MainWindow();
                mainWindow.Show();
                this.Close();
            }
            else
            {
                //MessageBox.Show();
                MessageBox mb = new MessageBox(false, "Логин или пароль введены неправильно.");
                mb.ShowDialog();
            }
        }
        }
}
