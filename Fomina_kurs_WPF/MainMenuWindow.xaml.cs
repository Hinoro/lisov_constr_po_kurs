﻿using Fomina_kurs_WPF.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Fomina_kurs_WPF
{
    /// <summary>
    /// Логика взаимодействия для MainMenuWindow.xaml
    /// </summary>
    public partial class MainMenuWindow : Window
    {
        FitnessContext db;
        int clientId;
        public MainMenuWindow(/*int user_id = 2*/)
        {
            InitializeComponent();
            db = new FitnessContext();
            clientId = 2;
            Frame.Content = new Page1(Frame, clientId);
        }

        private void Button_Abonements_Click(object sender, RoutedEventArgs e)
        {
            this.Title = "ФК For me now - Список абонементов";
            Frame.Content = new Page1(Frame, clientId);
        }
        private void Button_Trainers_Click(object sender, RoutedEventArgs e)
        {
            this.Title = "ФК For me now - Список тренеров";
            Frame.Content = new TrainersPage(Frame, clientId);
        }
        private void Button_AddAmonement_Click(object sender, RoutedEventArgs e)
        {
            AddAbonementForm addAbonementForm = new AddAbonementForm(clientId);
            if (addAbonementForm.ShowDialog() == true)
            {
                Frame.Content = new TrainersPage(Frame, clientId);
                MessageBox mb = new MessageBox(true, "Абонемент забронирован!");
                mb.ShowDialog();
            }
        }
    }
}
