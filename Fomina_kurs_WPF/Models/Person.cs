namespace Fomina_kurs_WPF.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Person")]
    public partial class Person
    {
        [Key]
        public int User_id { get; set; }

        [Required]
        [StringLength(60)]
        public string FIO { get; set; }

        public virtual User User { get; set; }
    }
}
