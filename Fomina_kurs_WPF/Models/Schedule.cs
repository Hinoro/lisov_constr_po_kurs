namespace Fomina_kurs_WPF.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Schedule")]
    public partial class Schedule
    {
        public int Id { get; set; }

        public int Abonement_id { get; set; }

        public int Period_Id { get; set; }

        [Column(TypeName = "date")]
        public DateTime Date { get; set; }

        public virtual Abonement Abonement { get; set; }

        public virtual PERIOD PERIOD { get; set; }
    }
}
