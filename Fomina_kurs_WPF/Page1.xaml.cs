﻿using Fomina_kurs_WPF.BLL;
using Fomina_kurs_WPF.Models;
using Fomina_kurs_WPF.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Fomina_kurs_WPF
{
    /// <summary>
    /// Логика взаимодействия для Page1.xaml
    /// </summary>
    public partial class Page1 : Page
    {
        Frame frame;
        Page2 page2;
        int clientId;
        private int trainer_id = 0;
        private int period_id = 0;
        private DateTime dateTime;

        public Page1(Frame fr, int client_id)
        {
            InitializeComponent();
            frame = fr;
            clientId = client_id;
            this.DataContext = new AppViewModel(clientId);
        }

        public Page1(Frame fr, int client_id, int trainer_id, int period_id, DateTime dateTime)
        {
            InitializeComponent();
            frame = fr;
            clientId = client_id;

            this.trainer_id = trainer_id;
            this.period_id = period_id;
            this.dateTime = dateTime;
            this.DataContext = new AppViewModel(clientId, trainer_id);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {

        }
        private void abonementsList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            AbonementModel abon = (abonementsList.SelectedItem as AbonementModel);
            ////System.Windows.MessageBox.Show(abon.Client_id.ToString());
            if (period_id == 0)
            {
                NavigationService nav = NavigationService.GetNavigationService(this);
                page2 = new Page2(frame, abon.Id);
                nav.Navigate(page2);
            } else
            {
                AddVisitingForm addVisitingForm = new AddVisitingForm(abon.Id, period_id, trainer_id, dateTime);
                if (addVisitingForm.ShowDialog() == true)
                {
                    TrainerSchedulePage trainerSchedulePage = new TrainerSchedulePage(frame, clientId, trainer_id);
                    frame.Content = trainerSchedulePage;
                    MessageBox mb = new MessageBox(true, "Посещение забронировано!");
                    mb.ShowDialog();
                }
            }
        }

        //public void ShowUsers()
        //{
        //    this.DataContext = new AppViewModel();
        //}
    }
}
