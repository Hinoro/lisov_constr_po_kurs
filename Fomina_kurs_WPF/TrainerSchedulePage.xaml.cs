﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Fomina_kurs_WPF.ViewModel;


namespace Fomina_kurs_WPF
{
    /// <summary>
    /// Логика взаимодействия для TrainerSchedulePage.xaml
    /// </summary>
    public partial class TrainerSchedulePage : Page
    {
        Frame frame;
        CommandBinding commandBinding;
        int trainerId;
        int clientId;
        public TrainerSchedulePage(Frame fr, int client_id, int trId)
        {
            trainerId = trId;
            clientId = client_id;
            // создаем привязку команды
            commandBinding = new CommandBinding();
            // устанавливаем команду
            commandBinding.Command = ApplicationCommands.New;
            // устанавливаем метод, который будет выполняться при вызове команды
            commandBinding.Executed += CommandBinding_Executed;
            // добавляем привязку к коллекции привязок элемента Button
            InitializeComponent();
            frame = fr;
            this.DataContext = new ScheduleTrainerViewModel(clientId, trId, DateTime.Now);
            //dataGridScheduleTrainer.Columns[1].Header = CultureInfo.GetCultureInfo("ru-RU").DateTimeFormat.GetDayName(DateTime.Now.DayOfWeek);
        }
        private void Cell_Click(object sender, RoutedEventArgs e)
        {
            var content = ((e.Source as Button).Content as Label).Content.ToString();
            //System.Windows.MessageBox.Show(keyword);
            //var cellInfo = dataGridScheduleTrainer.SelectedCells[0];
            //string content = ((cellInfo.Column.GetCellContent(cellInfo.Item) as Button).Content as Label).Content.ToString();

            //var content = cellInfo.Column.GetCellContent(cellInfo.Item);
            int period_id = Int32.Parse(content.Substring(0, content.IndexOf("|")));
            DateTime dateTime = DateTime.Parse(content.Substring(content.IndexOf("|") + 1));
            //AddVisitingForm addVisitingForm = new AddVisitingForm(abonementId);
            //if (addVisitingForm.ShowDialog() == true)
            //{
            //    this.DataContext = new ScheduleViewModel(abonementId);
            //}
            Page1 page1 = new Page1(frame, clientId, trainerId, period_id, dateTime);
            frame.Content = page1;
            //System.Windows.MessageBox.Show(content.ToString());
        }
        private void CommandBinding_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            
        }
    }
}
