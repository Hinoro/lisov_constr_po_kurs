﻿using Fomina_kurs_WPF.BLL;
using Fomina_kurs_WPF.Models;
using Fomina_kurs_WPF.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Fomina_kurs_WPF
{
    /// <summary>
    /// Логика взаимодействия для TrainersPage.xaml
    /// </summary>
    public partial class TrainersPage : Page
    {
        Frame frame;
        TrainerSchedulePage trainerSchedulePage;
        private int clientId;

        public TrainersPage(Frame fr, int client_id)
        {
            InitializeComponent();
            frame = fr;
            clientId = client_id;
            this.DataContext = new TrainerViewModel();
        }

        private void abonementsList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            TrainerModel trainer = (trainersList.SelectedItem as TrainerModel);
            ////System.Windows.MessageBox.Show(abon.Client_id.ToString());
            NavigationService nav = NavigationService.GetNavigationService(this);
            trainerSchedulePage = new TrainerSchedulePage(frame, clientId, trainer.User_id);
            nav.Navigate(trainerSchedulePage);
        }
    }
}
