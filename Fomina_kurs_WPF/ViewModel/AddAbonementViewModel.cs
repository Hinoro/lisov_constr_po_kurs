﻿using Fomina_kurs_WPF.BLL;
using Fomina_kurs_WPF.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace Fomina_kurs_WPF.ViewModel
{
    class AddAbonementViewModel : INotifyPropertyChanged
    {
        private FitnessContext db;
        int clientId;
        private Service selectedService = null;
        private int selectedExCount = 0;
        private Trainer selectedTrainer = null;
        public ObservableCollection<Service> Services { get; set; }
        public ObservableCollection<Trainer> Trainers { get; set; }
        public List<int> ExCounts { get; set; }
        public Service SelectedService
        {
            get { return selectedService; }
            set
            {
                selectedService = value;
                List<Trainer> tmpList = db.Trainer.Where(t => t.Service_id == selectedService.Id).ToList();
                Trainers.Clear();
                tmpList.ForEach(t =>
                {
                    Trainers.Add(t);
                });
                SelectedTrainer = Trainers[0];
                CalculatePrice();
                OnPropertyChanged("SelectedService");
            }
        }
        private int price = 0;
        public int Price { 
            get
            {
                return price;
            } 
            set {
                price = value;
                OnPropertyChanged("Price");

            }
        }
        private void CalculatePrice()
        {
            if (SelectedService != null && SelectedExCount != 0)
            Price = selectedService.Price_for_one_time * selectedExCount;
        }

        public int SelectedExCount
        {
            get { return selectedExCount; }
            set
            {
                selectedExCount = value;
                CalculatePrice();
                OnPropertyChanged("SelectedExCount");
            }
        }
        public Trainer SelectedTrainer
        {
            get { return selectedTrainer; }
            set
            {
                selectedTrainer = value;
                CalculatePrice();
                OnPropertyChanged("SelectedTrainer");
            }
        }
        
        private RelayCommand cmd;
        public RelayCommand InsertCommand
        {
            get
            {
                if (cmd == null) cmd = new RelayCommand(obj => {
                    InsertAbonement();
                    //он злой,  он очень плохой, он змея, крокодил, носорог, китобой 
                });

                return cmd;
            }
            set { 
                cmd = value; 
            }
        }
        private void InsertAbonement()
        {
            db.Abonement.Add(new Abonement() { Client_id = clientId, Service_id = selectedService.Id, Trainer_id = selectedTrainer.User_id, Exercise_count = selectedExCount, Price = price, Date_of_sale = DateTime.Now, Status_id = 2 });
            db.SaveChanges();
        }
        public AddAbonementViewModel(int client_id)
        { 
            db = new FitnessContext();
            clientId = client_id;
            ExCounts = new List<int>();
            for (int i = 5; i <= 20; i+= 5)
            {
                ExCounts.Add(i);
            }
            SelectedExCount = ExCounts[0];
            Trainers = new ObservableCollection<Trainer>();
            Services = new ObservableCollection<Service>(db.Service.ToList());
            SelectedService = Services[0];
        }
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }
    }
}
