﻿using Fomina_kurs_WPF.BLL;
using Fomina_kurs_WPF.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Fomina_kurs_WPF.ViewModel
{
    class AddVisitingModel : INotifyPropertyChanged
    {
        private int abonementId;
        private FitnessContext db;
        private DateTime date;
        private PERIOD selectedPeriod;
        public ObservableCollection<PERIOD> FreePeriods { get; set; }
        public PERIOD SelectedPeriod
        {
            get { return selectedPeriod; }
            set
            {
                selectedPeriod = value;
                OnPropertyChanged("SelectedPeriod");
            }
        }
        public DateTime Date
        {
            get { return date; }
            set { 
                date = value;
                var bookedPeriodsSelectedDay = db.Schedule.Where(s => s.Date == date).Select(s => s.Period_Id).ToArray();
                var freePeroidsList = db.PERIOD.Where(p => !bookedPeriodsSelectedDay.Contains(p.Id)).ToList();
                FreePeriods.Clear();
                freePeroidsList.ForEach(fp =>
                {
                    FreePeriods.Add(fp);
                });
                SelectedPeriod = freePeroidsList[0];
                //System.Windows.MessageBox.Show(FreePeroids.Count.ToString());

                OnPropertyChanged("Date");
            }
        }
        private RelayCommand cmd;
        public RelayCommand InsertCommand
        {
            get
            {
                if (cmd == null) cmd = new RelayCommand(obj => {
                    InsertVisiting();
                    //он злой,  он очень плохой, он змея, крокодил, носорог, китобой 
                });

                return cmd;
            }
            set { 
                cmd = value; 
            }
        }
        private void InsertVisiting()
        {
            db.Schedule.Add(new Schedule() { Abonement_id = abonementId, Period_Id = selectedPeriod.Id, Date = date });
            db.SaveChanges();

        }
        public AddVisitingModel(int abonId)
        { 
            db = new FitnessContext();
            abonementId = abonId;
            FreePeriods = new ObservableCollection<PERIOD>();
            //cmd = new RelayCommand(x => InsertVisiting());
            Date = DateTime.Now;
        }

        public AddVisitingModel(int abonId, int period_id, DateTime dateTime)
        {
            db = new FitnessContext();
            abonementId = abonId;
            FreePeriods = new ObservableCollection<PERIOD>();
            //cmd = new RelayCommand(x => InsertVisiting());
            Date = dateTime;
            SelectedPeriod = db.PERIOD.Find(period_id);
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }
    }
}
