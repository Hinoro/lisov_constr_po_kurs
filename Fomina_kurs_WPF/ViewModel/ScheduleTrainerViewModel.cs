﻿using Fomina_kurs_WPF.BLL;
using Fomina_kurs_WPF.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Fomina_kurs_WPF.ViewModel
{
    class ScheduleTrainerViewModel : INotifyPropertyChanged
    {
        int trainerId { get; }

        private DateTime startDate;
        private FitnessContext db;
        public ObservableCollection<TrainerModel> TrainerSchedule { get; set; }
        public ObservableCollection<Schedule> Schedule { get; set; }
        public ObservableCollection<ScheduleWeek> TestSchedule { get; set; }
        private RelayCommand cmd;
        public RelayCommand Cell_Click
        {
            get
            {
                if (cmd == null) cmd = new RelayCommand(obj => {
                    System.Windows.MessageBox.Show("oierferlf");
                });

                return cmd;
            }
            set
            {
                cmd = value;
            }
        }
        public ScheduleTrainerViewModel(int client_id, int trId, DateTime start_date)
        {
            trainerId = trId;
            startDate = start_date;
            db = new FitnessContext();
            Schedule = new ObservableCollection<Schedule>();
            TrainerSchedule = new ObservableCollection<TrainerModel>();
            var schedulesList = db.Schedule.Where(s => s.Abonement.Trainer_id == trId).ToList();
            schedulesList.ForEach(sch =>
            {
                TrainerSchedule.Add(new TrainerModel(db.Trainer.Find(trId)));
            });
            schedulesList.ForEach(sch =>
            {
                Schedule.Add(sch);
            });
            TestSchedule = new ObservableCollection<ScheduleWeek>();
            db.PERIOD.ToList().ForEach(period =>
            {
                TestSchedule.Add(new ScheduleWeek(client_id, trId, period.Id, DateTime.Now.StartOfWeek(DayOfWeek.Monday)));
            });
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }
    }
    public static class DateTimeExtensions
    {
        public static DateTime StartOfWeek(this DateTime dt, DayOfWeek startOfWeek)
        {
            int diff = (7 + (dt.DayOfWeek - startOfWeek)) % 7;
            return dt.AddDays(-1 * diff).Date;
        }
    }
}
